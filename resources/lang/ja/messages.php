<?php

return [
    'Login' => 'Conecte-se',
    'E-Mail Address' => 'endereço de e-mail',
    'Password' => 'senha',
    'Remember Me' => 'Lembre-se de suas informações de login',
    'Logout' => 'Sair',
    'Register' => 'inscrever-se',
    'Name' => 'nome',
    'Password' => 'senha',
    'Confirm Password' => 'senha（Para confirmação）',
];