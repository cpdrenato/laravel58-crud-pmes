<!-- ソリューション企業向けEdição de perfil画面　-->
@extends('layouts.common')
@section('title', 'Edição de perfil')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 mx-auto">
                <h2>Edição de perfil</h2>
                <form action="{{ action('SolutionProfileController@update', ['id' => $my_profile->id]) }}" method="post" enctype="multipart/form-data">
                    <!-- エラーmensagem  の表示 -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                            Existe um problema com a entrada. Por favor, entre novamente.
                        </div>
                    @endif
                    <!--　エラーがあれば編集中の内容を表示（全項目）　-->
                    <div class="form-group row">
                        <label class="col-md-3" >Nome da empresa (nome público)</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="public_name" value="{{ old('public_name', $my_profile->public_name) }}">
                            @if ($errors->has('public_name'))
                            <div class="text-danger">
                                {{$errors->first('public_name')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Logotipo da empresa</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control-file" name="logo_image">
                            @if ($errors->has('logo_image'))
                            <div class="text-danger">
                                {{$errors->first('logo_image')}}
                            </div>
                            @endif
                            <div class="form-text text-info">
                                @isset($my_profile->logo_image)
                                設定中: <img src="{{ old('logo_image', $my_profile->logo_image) }}" alt="ロゴimagem">
                                @endisset
                                @empty($my_profile->logo_image)
                                <p>imagem não está definida</p>
                                @endempty
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="delete[]" value="{{$my_profile->logo_image }}">Remover imagem
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Nome da região</label></label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="area" value="{{ old('area', $my_profile->area) }}">
                            @if ($errors->has('area'))
                            <div class="text-danger">
                                {{$errors->first('area')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Morada</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="address" rows="3">{{ old('address', $my_profile->address) }}</textarea>
                            @if ($errors->has('address'))
                            <div class="text-danger">
                                {{$errors->first('address')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">número de telefone</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number', $my_profile->phone_number) }}">
                            @if ($errors->has('phone_number'))
                            <div class="text-danger">
                                {{$errors->first('phone_number')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Site oficial </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="url" value="{{ old('url', $my_profile->url) }}">
                            @if ($errors->has('url'))
                            <div class="text-danger">
                                {{$errors->first('url')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Palavras-chave da solução<br>(Principais áreas de negócios)</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="solution_keyword" row="3">{{ old('solution_keyword', $my_profile->solution_keyword) }}</textarea>
                            @if ($errors->has('solution_keyword'))
                            <div class="text-danger">
                                {{$errors->first('solution_keyword')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Conteúdo da solução<br>(Detalhes da empresa)</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="solution_detail" row="10">{{ old('solution_detail', $my_profile->solution_detail) }}</textarea>
                            @if ($errors->has('solution_detail'))
                            <div class="text-danger">
                                {{$errors->first('solution_detail')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Conquistas de solução<br>(Desempenho dos negócios)</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="solution_performance" row="10">{{ old('solution_performance', $my_profile->solution_performance) }}</textarea>
                            @if ($errors->has('solution_performance'))
                            <div class="text-danger">
                                {{$errors->first('solution_performance')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Imagem sobre a solução </label>
                        <div class="col-md-9">
                            <input type="file" class="form-control-file" name="solution_image">
                            @if ($errors->has('solution_image'))
                            <div class="text-danger">
                                {{$errors->first('solution_image')}}
                            </div>
                            @endif
                            <div class="form-text text-info">
                                @isset($my_profile->solution_image)
                                設定中: <img src="{{ old('solution_image', $my_profile->solution_image) }}" alt="Imagem sobre a solução ">
                                @endisset
                                @empty($my_profile->solution_image)
                                <p>imagem não está definida</p>
                                @endempty
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="delete[]" value="{{$my_profile->solution_image}}">Remover imagem
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">mensagem </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="message" row="10">{{ old('message', $my_profile->message) }}</textarea>
                            @if ($errors->has('message'))
                            <div class="text-danger">
                                {{$errors->first('message')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Da pessoa responsávelmensagem </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="contact_message" row="10">{{ old('contact_message', $my_profile->contact_message) }}</textarea>
                            @if ($errors->has('contact_message'))
                            <div class="text-danger">
                                {{$errors->first('contact_message')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Sobre o responsávelimagem</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control-file" name="contact_image">
                            @if ($errors->has('contact_image'))
                            <div class="text-danger">
                                {{$errors->first('contact_image')}}
                            </div>
                            @endif
                            <div class="form-text text-info">
                                @isset($my_profile->contact_image)
                                設定中: <img src="{{ old('contact_image', $my_profile->contact_image) }}" alt="Sobre o responsávelimagem">
                                @endisset
                                @empty($my_profile->contact_image)
                                <p>imagem não está definida</p>
                                @endempty
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input type="checkbox" class="form-check-input" name="delete[]" value="{{ $my_profile->contact_image }}">Remover imagem
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Gerente endereço de e-mail</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="contact_email" value="{{ old('contact_email', $my_profile->contact_email) }}">
                            @if ($errors->has('contact_email'))
                            <div class="text-danger">
                                {{$errors->first('contact_email')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-10">
                        <input type="hidden" name="id" value="{{ $my_profile->id }}">
                        <input type="hidden" name="user_id" value="{{ $my_profile->user_id }}">
                        {{ csrf_field() }}
                        <div class="form-group row mb-0">
                            <div class="text-center col-md-6 offset-md-4">
                                <input type="submit" class="btn btn-primary" value="更新">
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
