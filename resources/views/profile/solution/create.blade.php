<!-- ソリューション企業向けRegistro de perfil画面 -->

@extends('layouts.common')
@section('title', 'Registro de perfil')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 mx-auto">
                <h2>Registro de perfil（Para empresas Brasileiras）</h2>
                <form action="{{ action('SolutionProfileController@create') }}" method="post" enctype="multipart/form-data">
                    <!-- エラーmensagem  の表示 -->
                    @if (count($errors) > 0)
                        <div class="alert alert-danger" role="alert">
                            Existe um problema com a entrada. Por favor, entre novamente.
                        </div>
                    @endif
                    <div class="form-group row">
                        <label class="col-md-3">Nome da empresa (nome público)</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="public_name" value="{{ old('public_name') }}" placeholder="Exemplo: ○ △ Co., Ltd.">
                            @if ($errors->has('public_name'))
                            <div class="text-danger">
                                {{$errors->first('public_name')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Logotipo da empresa</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control-file" name="logo_image">
                            <small class="input_condidion">*Formato: jpg, jpeg, png</small>
                            <small class="input_condidion">*Tamanho máximo: 250 x 250px</small>
                            @if ($errors->has('logo_image'))
                            <div class="text-danger">
                                {{$errors->first('logo_image')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Nome da região</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="area" value="{{ old('area') }}" placeholder="Exemplo: Kanto">
                            @if ($errors->has('area'))
                            <div class="text-danger">
                                {{$errors->first('area')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Morada</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="address" rows="3" placeholder="Exemplo: ○○ prefeitura □□ cidade ...">{{ old('address') }}</textarea>
                            @if ($errors->has('address'))
                            <div class="text-danger">
                                {{$errors->first('address')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">número de telefone<br>(Por favor, insira apenas números na metade da largura)</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="phone_number" value="{{ old('phone_number') }}" placeholder="exemplo: 0000000000">
                            @if ($errors->has('phone_number'))
                            <div class="text-danger">
                                {{$errors->first('phone_number')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Site oficial </label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="url" value="{{ old('url') }}" placeholder="exemplo: https://...">
                            @if ($errors->has('url'))
                            <div class="text-danger">
                                {{$errors->first('url')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Palavras-chave da solução<br>(Principais áreas de negócios)</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="solution_keyword" row="3" placeholder="exemplo: Usinagem de precisão, equipamentos médicos ...">{{ old('solution_keyword') }}</textarea>
                            @if ($errors->has('solution_keyword'))
                            <div class="text-danger">
                                {{$errors->first('solution_keyword')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Conteúdo da solução<br>(Detalhes da empresa)</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="solution_detail" row="10" placeholder="exemplo: Recebemos pedidos de fabricantes de dispositivos médicos e fabricamos e processamos equipamentos de precisão.">{{ old('solution_detail') }}</textarea>
                            @if ($errors->has('solution_detail'))
                            <div class="text-danger">
                                {{$errors->first('solution_detail')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Conquistas de solução<br>(Desempenho dos negócios)</label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="solution_performance" row="10" placeholder="exemplo: タイの医療メーカーからの受注を開始しました。">{{ old('solution_performance') }}</textarea>
                            @if ($errors->has('solution_performance'))
                            <div class="text-danger">
                                {{$errors->first('solution_performance')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Sobre a solução<br>imagem</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control-file" name="solution_image">
                            <small class="input_condidion">*Formato: jpg, jpeg, png<br></small>
                            <small class="input_condidion">*Tamanho máximo: 250 x 250px</small>
                            @if ($errors->has('solution_image'))
                            <div class="text-danger">
                                {{$errors->first('solution_image')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">mensagem </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="message" row="10" placeholder="exemplo: Perspectivas e planos futuros">{{ old('message') }}</textarea>
                            @if ($errors->has('message'))
                            <div class="text-danger">
                                {{$errors->first('message')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Da pessoa responsávelmensagem </label>
                        <div class="col-md-9">
                            <textarea class="form-control" name="contact_message" row="10">{{ old('contact_message') }}</textarea>
                            @if ($errors->has('contact_message'))
                            <div class="text-danger">
                                {{$errors->first('contact_message')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Sobre o responsávelimagem</label>
                        <div class="col-md-9">
                            <input type="file" class="form-control-file" name="contact_image">
                            <small class="input_condidion">*Formato: jpg, jpeg, png<br></small>
                            <small class="input_condidion">*Tamanho máximo: 250 x 250px</small>
                            @if ($errors->has('contact_image'))
                            <div class="text-danger">
                                {{$errors->first('contact_image')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3">Gerente endereço de e-mail</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="contact_email" value="{{ old('contact_email') }}">
                            @if ($errors->has('contact_email'))
                            <div class="text-danger">
                                {{$errors->first('contact_email')}}
                            </div>
                            @endif
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="form-group row mb-0">
                        <div class="text-center col-md-6 offset-md-4">
                            <input type="submit" class="btn btn-primary" value="registro">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
