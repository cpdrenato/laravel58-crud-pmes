<!-- お悩み企業お問合わせ画面 -->
@extends('layouts.common')
@section('title', 'Página de inquérito')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9 mx-auto">
                <div class="card my-3">
                    <div class="card-header">
                        <h4>investigação</h4>
                    </div>
                    <div class="body px-2 py-2">
                        <form action="{{ action('ChallengeContactController@confirm') }}" method="post">
                            <!--エラーmensagem の表示 -->
                            @if (count($errors) > 0)
                            <div class="alert alert-danger" role="alert">
                                Existe um problema com a entrada. Por favor, entre novamente.
                            </div>
                            @endif
                            <div class="form-group row mt-1">
                                <!-- A empresa que você deseja entrar em contato名の表示 -->
                                <label class="control-label col-md-3 mx-auto">A empresa que você deseja entrar em contato</label>
                                <div class="col-md-9">
                                    <!-- A empresa que você deseja entrar em contatoのid　-->
                                    <input class="form-control" type="hidden" name="challenge_id" id="InputChallengeId" value="{{ old('challenge_id', $challenge_id) }}">
                                    <input class="form-control" name="recipient_name" type="text" id="InputRecipientName" value="{{ old('recipient_name', $recipient_name) }}" readonly> 
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3 mx-auto">Itens de inquérito</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="subject">
                                        <option value="" selected="selected">Por favor selecione</option>
                                        @foreach ($subjects as $subject)
                                        <option value="{{ $subject }}" @if(old('subject')== $subject) selected @endif>{{ $subject }}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('subject'))
                                    <div class="text-danger">
                                        {{ $errors->first('subject') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3 mx-auto">nome</label>
                                <div class="col-md-9">
                                    
                                    <input class="form-control" type="hidden" name="user_id" id="InputUserId" value="{{ old('user_id', $user_id) }}">
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                                    @if ($errors->has('name'))
                                    <div class="text-danger">
                                        {{ $errors->first('name') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3 mx-auto">endereço de e-mail</label>
                                <div class="col-md-9">
                                    <input type="text" class="form-control" name="email" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                    <div class="text-danger">
                                        {{ $errors->first('email') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="control-label col-md-3 mx-auto">Conteúdo do inquérito</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" name="content" row="10">{{  old('content') }}</textarea>
                                    @if ($errors->has('content'))
                                    <div class="text-danger">
                                        {{ $errors->first('content') }}
                                    </div>
                                    @endif
                                </div>
                            </div>
                            {{ csrf_field() }}
                            <div class="text-right">
                                <input type="submit" class="btn btn-primary my-2 mx-2" value="Verificação">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
