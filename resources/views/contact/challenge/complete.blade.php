<!-- お悩み企業Inquérito concluído画面 -->
@extends('layouts.common')
@section('title', 'Inquérito concluído')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h4>Inquérito concluído</h4>
                    </div>
                    <div class="card-body">
                        <p>Obrigado por sua pergunta</p>
                    </div>
                </div>
                <!--ホーム画面へRetornaボタン-->
                <div class="content px-2 py-2">
                    <div class="text-center">
                        <a href="{{ route('home') }}" class="btn btn-success">De volta ao topo</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
