<!--ソリューション企業一覧 -->
@extends('layouts.common')
@section('title', 'Lista de soluções')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-8 mx-auto">
                <!-- palavra-chaveprocurar　-->
                <div class="content px-2 py-2">
                    <form action="{{ action('SolutionBoardController@index') }}" method="get">
                        <div class="form-group row">
                            <label class="col-md-2">palavra-chave</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="search_keyword" value="{{ $search_keyword }}">
                            </div>
                            <div class="col-md-2">
                                {{ csrf_field() }}
                                <input type="submit" class="btn btn-primary" value="procurar">
                            </div>
                        </div>
                    </form>
                </div>
                <!-- 一覧表示 -->
                <div class="board-index col-md-8 mx-auto">
                    <div class="content px-2 py-2">
                        <h4>Lista de soluções</h4>
                        @if(count($solution_boards) > 0)
                        @foreach($solution_boards as $board)
                        <div class="media row">
                            <div class="media-image col my-3 mx-auto">
                                @if($board->solution_image != null)
                                <!--Exibir imagens sobre a solução-->
                                <img src="{{ $board->solution_image }}" class="img-fluid text-center thumbnail mx-3" alt="Exibir imagens sobre a solução" data-toggle="modal" data-target="#image-modal" style="cursor:pointer">
                                <!--モーダルでimagemの表示-->
                                <div class="modal fade" id="image-modal" tabindex="-1" role="dialog" aria-labelledby="enlargeImgModalLabel">
                                    <div class="modal-dialog modal-md">
                                        <div class="modal-content text-center">
                                            <img src="{{ $board->solution_image }}" width="100%" height="100%" data-dismiss="modal">
                                        </div>
                                    </div>
                                </div>
                                @else
                                <img src="{{ $no_image }}" class=" text-center mx-3">
                                @endif
                            </div>
                            <!--xs, smサイズでは改行-->
                            <div class="d-block d-sm-none w-100"></div>
                            <div class="media-body col px-2 my-3 mx-auto">
                                <h5 class="mt-0">Solução palavra-chave</h5>
                                <p>{!! nl2br($board->solution_keyword) !!}</p>
                                <h5 class="mt-0">Nome da empresa de solução</h5>
                                <p>{{ $board->public_name }}</p>
                                <div class="text-right">
                                    <a href="{{ action('SolutionBoardController@show', ['id' => $board->id]) }}" class="btn btn-primary">Para a página de detalhes </a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <!--procurar条件に合うデータがなかった場合-->
                    @else
                    <div class="card no-board text-center my-3">
                        <div class="card-body">
                            <p class="card-text">Por favor, mude a condição procurar e procurar novamente</p>
                        </div>
                    </div>
                    @endif
                    </div>
                    <div class="pagination text-center my-3">
                        {{ $solution_boards->links() }}
                    </div>
                </div>
            </div>
            
        </div>
    </div>
@endsection
