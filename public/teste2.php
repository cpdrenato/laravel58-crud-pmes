<?php

// ini_set('memory_limit', '4M');

// $file = file_get_contents('numeros.txt');
$file = fopen('numeros.txt', 'rb');

var_dump($file); // resource(5) of type (stream)

// Usando fgets() podemos ler a linha da stream aberta, sem carregar o arquivo inteiro na memória:


ini_set('memory_limit', '4M');

$stream = fopen('numeros.txt', 'rb');

while (feof($stream) === false) {
    echo fgets($stream);
}

echo 'Memória utilizada: ' . (memory_get_peak_usage(true) / 1024 / 1024);

fclose($stream);