<?php

// Converti o script para usar Flysystem e também usa cerca de 2 MB de mem:

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;
use Illuminate\Support\Facades\Log;

//include __DIR__.'/vendor/autoload.php';
require __DIR__.'/../vendor/autoload.php';

$filesystem = new Filesystem(new Local(__DIR__));

const INPUT = __DIR__ . '/sax.mp4';
const OUTPUT = __DIR__ . '/out.mp4';
if (file_exists(OUTPUT)) {
    unlink(OUTPUT);
}

$handle = fopen(INPUT, 'rb');
//Log::debug($handle );
$filesystem->writeStream('out.mp4', $handle);


fclose($handle);
echo 'writeStream ok! \n';
var_dump(memory_get_peak_usage(true)/1024/1024);