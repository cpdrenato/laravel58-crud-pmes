<?php

// Converti o script para usar Flysystem e também usa cerca de 2 MB de mem:

use League\Flysystem\Adapter\Local;
use League\Flysystem\Filesystem;

//include __DIR__.'/vendor/autoload.php';
require __DIR__.'/../vendor/autoload.php';

$filesystem = new Filesystem(new Local(__DIR__));

const INPUT = __DIR__ . '/file.txt';
const OUTPUT = __DIR__ . '/out.txt';
if (file_exists(OUTPUT)) {
    unlink(OUTPUT);
}

$handle = fopen(INPUT, 'rb');

$filesystem->writeStream('out.txt', $handle);


fclose($handle);
var_dump(memory_get_peak_usage(true)/1024/1024);