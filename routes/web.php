<?php

Route::get('download/{rows?}', ['uses' => 'StreamedDownloadController@download']);

Route::get('/player', function () {
    $video = "video/route.mp4";
    $mime = "video/mp4";
    $title = "Video";

    return view('player')->with(compact('video', 'mime', 'title'));
});

Route::get('/video/{filename}', function ($filename) {
    // Pasta dos videos.
    $videosDir = base_path('resources/assets/videos');

    if (file_exists($filePath = $videosDir."/".$filename)) {
        $stream = new \App\Http\VideoStream($filePath);

        return response()->stream(function() use ($stream) {
            $stream->start();
        });
    }

    return response("File doesn't exists", 404);
});

/**
 * Registro de perfil、Minha página
 */
Route::group(['middleware' => 'auth'], function() {
    
    Route::get('profile/challenge/create', 'ChallengeProfileController@add');
    Route::post('profile/challenge/create', 'ChallengeProfileController@create');
    Route::get('profile/challenge/edit/{id}', 'ChallengeProfileController@edit');
    Route::post('profile/challenge/edit/{id}', 'ChallengeProfileController@update');
    Route::delete('profile/challenge/delete', 'ChallengeProfileController@delete');
    
    Route::get('profile/solution/create', 'SolutionProfileController@add');
    Route::post('profile/solution/create', 'SolutionProfileController@create');
    Route::get('profile/solution/edit/{id}', 'SolutionProfileController@edit');
    Route::post('profile/solution/edit/{id}', 'SolutionProfileController@update');
    Route::delete('profile/solution/delete', 'SolutionProfileController@delete');
    //Minha página
    Route::get('mypage', 'MypageController@show')->name('mypage');
    // Minha Editar as informações do usuário、更新
    Route::get('mypage/edit', 'UserController@edit');
    Route::post('mypage/update', 'UserController@update');
});
/**
 * 
 */
Route::group(['middleware' => 'auth'], function() {
    //
    Route::get('challenge/contact', 'ChallengeContactController@input');
    Route::post('challenge/contact/confirm', 'ChallengeContactController@confirm');
    Route::post('challenge/contact/complete', 'ChallengeContactController@complete');
    //
    Route::get('solution/contact', 'SolutionContactController@input');
    Route::post('solution/contact/confirm', 'SolutionContactController@confirm');
    Route::post('solution/contact/complete', 'SolutionContactController@complete');
});

/**
 * 
 */
 //
Route::get('solution/index', 'SolutionBoardController@index');
Route::get('solution/{id}', 'SolutionBoardController@show');
//Lista de preocupações、詳細
Route::get('challenge/index', 'ChallengeBoardController@index');
Route::get('challenge/{id}', 'ChallengeBoardController@show');

Route::get('/','HomeController@index')->name('home');

Auth::routes();


