<?php

namespace App;

class AwsFileUploader
{
    // Dependency Injected AWS S3 Connection:
    // @var League\Flysystem\EventableFilesystem
    protected $connection;

    public function putFile($localPath, $storagePath)
    {
        $stream = fopen($localPath, 'r+');

        $this->connection->addListener('before.putstream', function(Before $event) {
            // How do I allow stream_context_set_params to access the putstream context?
        });

        return $this->connection->putStream($storagePath, $stream);
    }
}