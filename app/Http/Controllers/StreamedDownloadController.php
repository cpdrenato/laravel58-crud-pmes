<?php

namespace App\Http\Controllers;

use Faker;
use App\VideoStream;
use App\StreamedDownload;
use Illuminate\Http\Request;
use Aws\S3\MultipartUploader;
use Aws\Exception\MultipartUploadException;

class StreamedDownloadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StreamedDownload  $streamedDownload
     * @return \Illuminate\Http\Response
     */
    public function show(StreamedDownload $streamedDownload)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StreamedDownload  $streamedDownload
     * @return \Illuminate\Http\Response
     */
    public function edit(StreamedDownload $streamedDownload)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StreamedDownload  $streamedDownload
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, StreamedDownload $streamedDownload)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StreamedDownload  $streamedDownload
     * @return \Illuminate\Http\Response
     */
    public function destroy(StreamedDownload $streamedDownload)
    {
        //
    }

    public function download($rows = 50000)
    {
        // callback function that writes to php://output
        $callback = function() use ($rows) {

            // Open output stream
            $handle = fopen('php://output', 'w');
    
            // Add CSV headers
            fputcsv($handle, [
                'Name',
                'Address', 
            ]);
    
            // Generate a faker instance
            $faker = Faker\Factory::create();
    
            // add the given number of rows to the file.
            for ($i=0; $i < $rows ; $i++) { 
                $row = [
                    $faker->name,
                    $faker->address,
                ];
                fputcsv($handle, $row);
            }
    
    
            // Close the output stream
            fclose($handle);

            
        };
        // build response headers so file downloads.
        $headers = [
            'Content-Type' => 'text/csv',
        ];
        // return the response as a streamed response.
        return response()->streamDownload($callback, 'download.csv', $headers);
    
    }

    public function uploadToS3($fromPath, $toPath)
    {
        $disk = Storage::disk('s3');
        $uploader = new MultipartUploader($disk->getDriver()->getAdapter()->getClient(), $fromPath, [
            'bucket' => Config::get('filesystems.disks.s3.bucket'),
            'key'    => $toPath,
        ]);

        try {
            $result = $uploader->upload();
            echo "Upload complete";
        } catch (MultipartUploadException $e) {
            echo $e->getMessage();
        }
    }

    public function vide($filePath)
    {
        $stream = new VideoStream($filePath);
        $stream->start();
    }
}
