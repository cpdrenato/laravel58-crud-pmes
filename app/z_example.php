<?php 

namespace App;

use \Illuminate\Database\Eloquent\Model;

class Post extends Model{
  use HasWithCountScope;
  
  public function comments() {
       return $this->hasMany(Comment::class);
  }
  
  public function votes() {
       return $this->hasMany(Vote::class);
  }
}

// USAGE 1

$posts = Post::withCount('comments')->get();

$posts[0]->comments_count; //total comments of the post

// USAGE 2
$posts = Post::withCount(['votes', 'comments' => function ($query) {
   $query->where('created_at', '>', date('yesterday'))
}])->get();

$posts[0]->comments_count; //total comments created after yesterday
$posts[0]->votes_count; //total votes of the posts

// USAGE 3
$posts = Post::withCount(['votes as total_votes', 'comments as total_comments' => function ($query) {
   $query->where('created_at', '>', date('yesterday'))
}])->get();

$posts[0]->total_comments; //total comments created after yesterday
$posts[0]->total_votes; //total votes of the posts