<?php
namespace S3;

use Aws\S3\S3Client;
use \Aws\Common\Exception\TransferException;

/**
 * Description of S3Client
 * @author Rana
 * @link http://codesamplez.com/programming/amazon-s3-get-multiple-objects-php
 */
class MyS3Client extends S3Client
{
    //put your code here
    public static function getObjects(Array $configs, S3Client $client)
    {
        $requests = array();
        $savePaths = array();
        foreach ($configs as $config) {
            $url             = "https://".$config["Bucket"].".s3.amazonaws.com/".$config["Key"];
            $request         = $client->get($url); 
            $requests[]      = $request;
            $savePaths[$url] = $config["saveAs"]; 
        }
        
        try {
            $responses = $client->send($requests);
        }
        catch(TransferException $e) {
            echo $e->getError();
        }

        foreach ($responses as $res) {                  
            $localPath = $savePaths[$res->getEffectiveUrl()];
                file_put_contents($localPath, $res->getBody(true));
        }
    }
}